import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createTheme, ThemeProvider } from '@material-ui/core';
import { GREEN } from './constants/color';

const theme = createTheme({
  typography: {
    fontFamily: " -apple-system,'Public Sans', 'Roboto',  sans-serif;",
    fontSize: 14
  },
  palette: {
    primary: {
      main: `${GREEN}`
    }
  }
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
