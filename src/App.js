import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { LoginForm } from 'features/Auth/Admin';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/admin/login" exact component={LoginForm} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
