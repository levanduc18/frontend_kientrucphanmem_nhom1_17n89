import {
  Button,
  Grid,
  Container,
  Box,
  Typography,
  InputAdornment,
  IconButton,
  Checkbox,
  Slide,
  Fade
} from '@material-ui/core';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

function LoginForm() {
  const classes = useStyles();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState(null);
  const [showPassword, setShowPassword] = useState(false);
  const [checkStatus, setCheckStatus] = useState(false);

  const handleChangeEmail = (event) => setEmail(event.target.value);

  const handleChangePassword = (event) => setPassword(event.target.value);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleCheckBoxChange = () => setCheckStatus(!checkStatus);
  return (
    <Container maxWidth="xl" className={classes.root}>
      <Grid container>
        <Slide direction="right" in timeout={1500}>
          <Grid item xs={12} sm={4} className={classes.imageGrid}>
            <Grid
              container
              direction="row"
              alignItems="center"
              justifyContent="center"
              className={classes.logoContainer}
            >
              <Box
                component="img"
                sx={{
                  height: '10%',
                  width: '10%'
                }}
                alt="Admin image introduction"
                src="https://blog-consumer.glassdoor.com/app/uploads/sites/2/illustration-find-a-job-youl-love.svg"
              ></Box>
              <Typography>
                <Box fontSize={20} fontWeight={900}>
                  ITJobFinder
                </Box>
              </Typography>
            </Grid>
            <Typography className={classes.imageTitle}>
              <Box fontSize={30} fontWeight={900}>
                Hi, Welcome Back
              </Box>
            </Typography>
            <Box
              className={classes.image}
              component="img"
              sx={{
                height: '45%',
                width: '100%'
              }}
              alt="Admin image introduction"
              src="https://pixinvent.com/demo/vuexy-bootstrap-laravel-admin-template/demo-4/images/pages/login-v2-dark.svg"
            ></Box>
          </Grid>
        </Slide>
        <Fade in timeout={2000}>
          <Grid item xs={12} sm={8} className={classes.loginForm}>
            <Grid className={classes.formContainer}>
              <Typography className={classes.formTilte}>
                <Box fontSize={27} fontWeight={600}>
                  Only for Admin
                </Box>
              </Typography>
              <Typography className={classes.describe}>
                <Box fontSize={15} fontWeight={400} color="#878787">
                  Please enter admin account.
                </Box>
              </Typography>
              <ValidatorForm>
                <TextValidator
                  fullWidth
                  variant="outlined"
                  className={classes.input}
                  label="Email address"
                  onChange={handleChangeEmail}
                  autoComplete="none"
                  name="email"
                  value={email}
                  validators={['required', 'isEmail']}
                  errorMessages={[
                    'Email is required',
                    'Email must be a valid email address'
                  ]}
                />
                <TextValidator
                  fullWidth
                  variant="outlined"
                  className={classes.input}
                  label="Password"
                  onChange={handleChangePassword}
                  name="password"
                  type={showPassword ? 'text' : 'password'}
                  value={password}
                  validators={[
                    'required',
                    'minStringLength:8',
                    'maxStringLength:255'
                  ]}
                  errorMessages={[
                    'Password is required',
                    'Password must be minimum of 8 characters ',
                    'Password must be maximum of 255 characters'
                  ]}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          edge="center"
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
                <Grid container direction="row" alignItems="center">
                  <Checkbox
                    checked={checkStatus}
                    onChange={handleCheckBoxChange}
                    color="primary"
                  ></Checkbox>
                  <Typography>
                    <Box fontSize={14} fontWeight={400}>
                      Remember me
                    </Box>
                  </Typography>
                </Grid>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  className={classes.button}
                  type="submit"
                >
                  Log in
                </Button>
              </ValidatorForm>
            </Grid>
          </Grid>
        </Fade>
      </Grid>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  },
  imageGrid: {
    boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
    height: '90vh',
    borderRadius: 10
  },
  imageTitle: {
    textAlign: 'center',
    padding: theme.spacing(5)
  },
  logoContainer: {
    margin: theme.spacing(5, 0, 0, 0)
  },
  image: {
    padding: theme.spacing(2),
    margin: theme.spacing(5, 0)
  },
  formContainer: {
    width: '60%',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  loginForm: {
    padding: theme.spacing(10)
  },
  describe: {
    margin: theme.spacing(1, 0, 2, 0)
  },
  button: {
    margin: theme.spacing(3, 0),
    padding: theme.spacing(1.5),
    boxShadow: 'rgb(0 171 85 / 24%) 0px 8px 16px 0px',
    borderRadius: 10
  },
  input: {
    margin: theme.spacing(2, 0),
    [`& fieldset`]: {
      borderRadius: 10
    }
  }
}));

export default LoginForm;
